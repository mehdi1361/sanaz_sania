# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-30 10:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Recepi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='link',
            field=models.CharField(max_length=250, verbose_name='link'),
        ),
        migrations.AlterField(
            model_name='banner',
            name='thumbnail',
            field=models.CharField(max_length=250, null=True, verbose_name='thumbnail'),
        ),
    ]
