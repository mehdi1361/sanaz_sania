from django.core.management.base import BaseCommand, CommandError
from Recepi.utils import Crawl


class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in range(1, 1000000):
            crawler = Crawl(i)
            crawler.run()
