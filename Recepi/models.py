from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    created_date = models.DateTimeField(_('created date'), auto_now_add=True)
    updated_date = models.DateTimeField(_('updated date'), auto_now_add=True)
    store_id = models.IntegerField(_('source'), default=0)

    class Meta:
        abstract = True


class Material(BaseModel):
    name = models.CharField(_('name'), max_length=50)
    amount = models.CharField(_('amount'), max_length=50, null=True)
    amount_value = models.CharField(_('amount value'), max_length=50, null=True)
    description = models.CharField(_('description '), max_length=50)

    class Meta:
        db_table = 'recipe_materials'
        verbose_name = 'recipe_material'
        verbose_name_plural = 'recipe_material'


class RecipeNote(BaseModel):
    title = models.CharField(_('verbose name'), max_length=50, null=True)
    computable = models.BooleanField(_('computable'), default=False)
    share_link = models.CharField(_('verbose name'), max_length=50)
    is_favorite = models.BooleanField(_('is favorite'), default=False)
    rate = models.IntegerField(_('rate'), default=0)
    lead = models.IntegerField(_('lead'), null=True)
    material = models.ManyToManyField(Material, verbose_name=_('materials'), related_name='materials')

    class Meta:
        db_table = 'recipe_note'
        verbose_name = 'recipe_note'
        verbose_name_plural = 'recipe_notes'


class Banner(BaseModel):
    banner_type = models.CharField(_('banner type'), max_length=50, default='image')
    thumbnail = models.CharField(verbose_name=_('thumbnail'), max_length=250, null=True)
    link = models.CharField(verbose_name=_('link'), max_length=250)
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='banners')

    class Meta:
        db_table = 'recipe_banner'
        verbose_name = 'recipe_banner'
        verbose_name_plural = 'recipe_banners'


class CookingProperties(BaseModel):
    cooking_time = models.IntegerField(_('cooking time'), default=0)
    preparation_time = models.IntegerField(_('preparation time'), default=0)
    rest_time = models.IntegerField(_('rest time'), default=0)
    people_count = models.IntegerField(_('people count'), default=0)
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='cooking_properties')

    class Meta:
        db_table = 'cooking_properties'
        verbose_name = 'cooking_property'
        verbose_name_plural = 'cooking_properties'


class Tag(BaseModel):
    text = models.CharField(_('text'), max_length=50)
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='tags')

    class Meta:
        db_table = 'recipe_tags'
        verbose_name = 'recipe_tag'
        verbose_name_plural = 'recipe_tags'


class Step(BaseModel):
    text = models.TextField(_('text'))
    picture = models.CharField(_('picture'), max_length=200, null=True, blank=True)
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('steps'), related_name='steps')

    class Meta:
        db_table = 'recipe_steps'
        verbose_name = 'recipe_step'
        verbose_name_plural = 'recipe_steps'


class Technic(BaseModel):
    title = models.CharField(_('title'), max_length=50)
    text = models.TextField(_('text'))
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='cooking_technic')

    class Meta:
        db_table = 'recipe_technics'
        verbose_name = 'recipe_technic'
        verbose_name_plural = 'recipe_technics'


class Tip(BaseModel):
    text = models.TextField(_('text'))
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='cooking_tips')

    class Meta:
        db_table = 'recipe_tips'
        verbose_name = 'recipe_tip'
        verbose_name_plural = 'recipe_tips'


class Alternative(BaseModel):
    text = models.TextField(_('text'))
    recipe = models.ForeignKey(RecipeNote, verbose_name=_('recipe'), related_name='cooking_alternatives')

    class Meta:
        db_table = 'recipe_alternatives'
        verbose_name = 'recipe_alternative'
        verbose_name_plural = 'recipe_alternatives'
