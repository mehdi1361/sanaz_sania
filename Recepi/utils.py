import requests
import json
from .models import RecipeNote, Tag, Step, Material, Technic, Tip, Banner


class Crawl(object):
    def __init__(self, resp_id):
        self.resp_id = resp_id
        self.url = "http://sanazsaniaapp.com"
        self.crawl_step = ['first', 'second', 'third']
        self.recipe_note = set()

    def call_api(self, step_url):
        querystring = {"ver": "1.0", "platform": "ios HTTP/1.1"}

        payload = "{\n\t\"Resepi_Id\":%s\n}" % self.resp_id
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request("POST", '%s/%s' %
                                    (self.url, step_url), data=payload, headers=headers, params=querystring)
        result = json.loads(response.text)
        return result

    def step_one(self):
        result = self.call_api('application/api/resepidetailfirst')
        if result['ID'] != '200':
            return False

        self.recipe_note, created = RecipeNote.objects.get_or_create(
            title=result['Info']['Title'],
            computable=result['Info']['Computable'],
            share_link=result['Info']['Share_Link'],
            is_favorite=result['Info']['isFavorite'],
            rate=result['Info']['Rate'],
            defaults={'store_id': result['Info']['Id']}
        )
        for bnr in result['Info']['Media']['Banner']:

            print("banner process...")
            thumb_file_name = bnr['Thumbnail'].split("/")[-1]
            link_file_name = bnr['Link'].split("/")[-1]

            thumbnail_response = requests.get(bnr['Thumbnail'])
            link_response = requests.get(bnr['Link'])

            with open("/home/mehdi/sanaz_pics/thumb/%s" % thumb_file_name, "wb") as f:
                f.write(thumbnail_response.content)

            with open("/home/mehdi/sanaz_pics/link/%s" % link_file_name, "wb") as f:
                f.write(link_response.content)

            bnr = Banner.objects.create(thumbnail= thumb_file_name, link=link_file_name, recipe=self.recipe_note)

        if result['Info']['Tags']:
            for tg in result['Info']['Tags']:
                tag, created = Tag.objects.get_or_create(
                    text=tg['Text'],
                    recipe=self.recipe_note,
                    defaults={'store_id': tg['Tag_Id']}
                )
        return True

    def step_two(self):
        if not self.recipe_note:
            self.recipe_note = RecipeNote.objects.get(store_id=self.resp_id)

        result = self.call_api('application/api/resepidetailsecond')
        if result['ID'] == '200':
            print('***************** steps *****************')
            if result['Info']['Steps']:
                for stp in result['Info']['Steps']:
                    print(stp)
                    step, created = Step.objects.get_or_create(
                        picture=stp['Picture'],
                        text=stp['Text'],
                        recipe=self.recipe_note,
                        defaults={'store_id': stp['Step_Id']}
                    )

            print('***************** materials *****************')
            if result['Info']['Materials']:
                for mtrl in result['Info']['Materials']:
                    print(mtrl)
                    material, created = Material.objects.get_or_create(
                        name=mtrl['Name'],
                        amount=mtrl['Amount'],
                        amount_value=mtrl['AmountValue'],
                        description=mtrl['Description'],
                        defaults={'store_id': mtrl['Material_Id']}
                    )
                    self.recipe_note.material.add(material)

    def step_three(self):
        if not self.recipe_note:
            self.recipe_note = RecipeNote.objects.get(store_id=self.resp_id)

        result = self.call_api('application/api/resepidetailthird')
        if result['ID'] == '200':
            print('***************** Techniques *****************')
            if result['Info']['Techniques']:
                for tec in result['Info']['Techniques']:
                    print(tec)
                    technic, created = Technic.objects.get_or_create(
                        title=tec['Title'],
                        text=tec['Text'],
                        recipe=self.recipe_note,
                        defaults={'store_id': tec['Technique_Id']}
                    )

            print('***************** Tips *****************')
            if result['Info']['Tips']:
                for ctip in result['Info']['Tips']:
                    print(ctip)
                    tip, created = Tip.objects.get_or_create(
                        text=ctip['Text'],
                        recipe=self.recipe_note,
                        defaults={'store_id': ctip['Tip_Id']}
                    )

            print('***************** alternative *****************')
            if result['Info']['Alternative']:
                for alt in result['Info']['Alternative']:
                    print(alt)
                    alternative, created = Technic.objects.get_or_create(
                        text=alt['Text'],
                        recipe=self.recipe_note,
                        defaults={'store_id': alt['Alternative_Id']}
                    )

    def run(self):
        if self.step_one():
            self.step_two()
            self.step_three()
        else:
            print("not found recipe...")
